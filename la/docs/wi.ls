<li>Day 1: </li>
<li>Getting Started and Getting Finished</li>
<li>9:00 am – 12:00 pm </li>
<li>Introductions, Writing Process Discussion, Productivity</li>
<li>12:00 – 1:00 </li>
<li>Lunch (working lunch; provided)</li>
<li>1:00 – 2:00 </li>
<li>Writing consultations</li>
<li>1:00 – 5:00 pm </li>
<li>Optional writing boot camp</li>
<li></li>
<li>Day 2: </li>
<li>Organizing You and Your Writing</li>
<li>9:00 am – 12:00 pm </li>
<li>Process Analysis, Organizational Methods, Lit Review</li>
<li>12:00 – 1:00 </li>
<li>Lunch (working lunch; provided)</li>
<li>1:00 – 2:00 </li>
<li>Writing consultations</li>
<li>1:00 – 5:00 pm </li>
<li>Optional writing boot camp</li>
<li></li>
<li>Day 3: </li>
<li>“The Evil P’s”</li>
<li>9:00 am – 12:00 pm </li>
<li>Grammatical Challenges, Clarity and Concision</li>
<li>12:00 – 1:00 </li>
<li>Lunch (working lunch; provided)</li>
<li>1:00 – 2:00 </li>
<li>Writing consultations</li>
<li>1:00 – 5:00 pm </li>
<li>Optional writing boot camp</li>
<li></li>
<li>Day 4: </li>
<li>Moving Forward</li>
<li>9:00 am – 12:00 pm </li>
<li>Source Management and Integration</li>
<li>12:00 – 1:00 </li>
<li>Lunch (working lunch; provided)</li>
<li>1:00 – 2:00 </li>
<li>Writing consultations</li>
<li>1:00 – 5:00 pm </li>
<li>Optional writing boot camp</li>
