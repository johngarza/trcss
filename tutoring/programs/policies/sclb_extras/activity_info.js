/**
 *  ==================================================
 *  SoftChalk LessonBuilder
 *  Activity activity_info.js
 *  Copyright 2002-2015 SoftChalk LLC
 *  All Rights Reserved.
 *
 *  http://www.softchalk.com
 *  ==================================================
 */

$(document).ready(function() {
	var overlay_div = undefined;
	$('.greymatter_show').click(function() {
		var element = $('#'+$(this).attr('ref'));
		overlay_div = element.parent();
		if(overlay_div.css('display') == '' || overlay_div.css('display') == 'none') {
			overlay_div.css('display','block');
			overlay_div.trigger('isVisible');
			$(window).trigger('resize');
		}
		return false;
	});
	$('.greymatter_hide').click(function() {
		$(this).parent().parent().fadeOut(150);
		overlay_div.trigger('isHidden');
	});
	$('.greymatter').click(function(e) {
		if($(e.target).hasClass('greymatter')) {
			$('.greymatter').fadeOut(150);
			overlay_div.trigger('isHidden');
		}
	});
});

q_done_a=new Array();
scoreQa=new Array();
score_a=new Array();
show_restart_a=new Array();
num_pages=19;
actOrder=new Array(3);
actOrder[0]=10;
actOrder[1]=11;
actOrder[2]=14;

//a_num=10;
a_value10=1;
scoreQa[10]=true;
q_done_a[10]=false;
score_a[10]=0;
show_restart_a[10]=false;

//a_num=11;
a_value11=0;
scoreQa[11]=false;
q_done_a[11]=false;
score_a[11]=0;
show_restart_a[11]=true;

//a_num=14;
a_value14=7;
scoreQa[14]=true;
q_done_a[14]=false;
score_a[14]=0;
show_restart_a[14]=false;

