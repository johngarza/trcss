/**
 *  ==================================================
 *  SoftChalk LessonBuilder
 *  Copyright 2003-2006 SoftChalk LLC
 *  All Rights Reserved.
 *
 *  http://www.softchalk.com
 *  ==================================================
 *	File date: May 1, 2006
 */


function answersWin(myFile, myID, winSpecs) {
	answersWindow = window.open(myFile, myID, winSpecs);
}


function activity_win_close() {
	window.opener = top;
	window.close();
}


// Chris' debugging method not needed
// but keep method name to avoid javascript errors
function readit() {}


// called from an activity pop up window
function cookit() {
	opener.cookit(arguments[0], arguments[1], arguments[2], arguments[3]);
}
