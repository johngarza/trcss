<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><!-- InstanceBegin template="/Templates/trcss-cfm.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<link rel="shortcut icon" href="/favicon.ico" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="author" content="The University of Texas at San Antonio, Web and Multimedia Services - 2010" />
<meta name="copyright" content="The University of Texas at San Antonio" />
<!-- InstanceBeginEditable name="meta" -->
<meta name="Description" content="The University of Texas at San Antonio" />
<meta name="Keywords" content="UTSA, University, University of Texas, University of Texas at San Antonio, San Antonio, Colleges, Texas, Premier Public Research, Texas San Antonio" />
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="title" -->
<title>Main Campus Schedules | Supplemental Instruction | Tom&aacute;s Rivera Center | UTSA | The University of Texas at San Antonio</title>
<!-- InstanceEndEditable -->
<link rel="stylesheet" type="text/css" href="/trcss/css/master.css" />
<link rel="stylesheet" type="text/css" href="/css/centers.css" />
<!--[if IE]><link rel="stylesheet" type="text/css" href="/css/ie.css" /><![endif]-->
<!--[if lte IE 7]><link rel="stylesheet" type="text/css" href="/css/ie76.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="/css/ie6.css" /><![endif]-->
<script type="text/javascript" src="/js/jquery-optimized-utsa.js"></script>
<script type="text/javascript" src="/js/custom.js"></script>



<cfinclude template="/trcss/inc-share/analytics/analytics.js">
<!-- InstanceBeginEditable name="head" -->


<!-- InstanceEndEditable -->
</head>
<body>
<!-- Branding /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="branding-blue">
  <cfinclude template="/trcss/inc-share/accessibility/screen-reader-skip.html">
  <cfinclude template="/trcss/inc/master/brandWrap-globalNav.html">

</div>
<!-- end branding -->
<!-- CENTER COLOR /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="center-color">
  <div id="center-head"><a name="acces-content" class="screen-reader"></a> <!-- InstanceBeginEditable name="title-navigation" --> <cfinclude template="/trcss/inc/trc-title-nav.html"><!-- InstanceEndEditable -->
    <div class="floatC"></div>
  </div>
</div>
<!-- end center color -->
<div id="content-white">
  <div id="center-body" class="clearfix">
    <div id="col-navigation" class="clearfix">
      <div class="nav-page"> <!-- InstanceBeginEditable name="page-navigation" --> 
      <cfinclude template="/trcss/inc/trc-side-nav.html"><!-- InstanceEndEditable --> </div>
    </div>
    <div id="center-secondary-body" class="clearfix"> <!-- InstanceBeginEditable name="content" -->  
  <cfquery name="MainCampusLeaders" datasource="SI-Leaders">
	SELECT Sub,Crse,Sec,Instructor,Day,Start,End, Location,Leader  
	FROM MainCampusLeaders
</cfquery>
<cfset ID = "Leader"  />
<cfset ID = "ID" />
<h1 align="center">Main Campus Tutoring Session Schedules</h1>
 <cfform action="MCSchedulesResults.cfm?ID=#ID#" method="post" enctype="application/x-www-form-urlencoded" name="pickme" preloader="no" id="myForm2" lang="en" dir="ltr">
<cfquery name="MCSub" datasource="Si-Leaders">
SELECT Sub 
FROM MCSub
ORDER BY Sub
</cfquery>
<cfloop index="item" list="#MainCampusLeaders.Leader#">
  </cfloop>
   <cfquery name="MCSub" datasource="Si-Leaders">
SELECT Sub  
FROM MCSub
</cfquery>
   <cfselect name="SubList" query="MCSub" display="Sub" value="Sub" datasource="Si-Leaders" selected = "selected" id="h3b">
   <option value=""></option>   
   </cfselect>  
 <div style="clear: both; padding: 15px 0px 0px 0px">  
             <div style="clear: both; float: left; padding: 0px `0px 0px 0px`">
               <cfinput type="submit" name="submit" id="btn_submit2" style="cursor: pointer;" dir="ltr" lang="en" value="OK">
             </div>  
            <div style="float: left">  
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<cfinput type="reset" name="reset" id="btn_reset" style="cursor: pointer;" dir="ltr" lang="en" value="CLEAR">               
             </div>  
         </div>

</cfform>

	<!-- InstanceEndEditable --> </div>
    <!-- end starter-content-->
    <div class="floatC"></div>
  </div>
  <!--end CollegeBody -->
  <!-- InstanceBeginEditable name="footer" --><!-- InstanceEndEditable --> </div>
<!--end contentWhite-->
<!-- Footer /////////////////////////////////////////////////////////////////////////////////////////////////////// -->
<div id="footer-blue">
  <div id="footer">
    <cfinclude template="/trcss/inc/master/footerWrap.html">
  </div>
</div>
</body>
<!-- InstanceEnd --></html>
