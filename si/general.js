function openCourse(evt, CourseName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the link that opened the tab
    document.getElementById(CourseName).style.display = "block";
    evt.currentTarget.className += " active";
}

$('#flagThis').click(function (event) {
        $('#reportThis').show("slow");
        event.preventDefault();
});


function openSchedule(campusName){
    document.getElementById("mainCampus").style.display = "none";
    document.getElementById("downtownCampus").style.display = "none";
    document.getElementById("note").style.display = "none"; 
    
    document.getElementById(campusName).style.display = "block";
}